Source: mango-kong
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Scarlett Moore <sgmoore@debian.org>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-alecthomas-kong-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/mango-kong
Vcs-Git: https://salsa.debian.org/go-team/packages/mango-kong.git
Homepage: https://github.com/alecthomas/mango-kong
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/alecthomas/mango-kong

Package: mango-kong
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: Mango (man page generator) integration for Kong (program)
 Mango (man page generator) integration for Kong
 .
 This package allows Kong (https://github.com/alecthomas/kong) packages
 to generate man pages using Mango (https://github.com/muesli/mango).
 .
 Simply add mangokong.ManFlag to your CLI struct:
 .
   var cli struct {
   	Man mangokong.ManFlag `help:"Write man page."`
   }
 .
 Example
 .
 This is what the included example (/example) looks like:
 .
 [Image: example.1] (/example.1.png)

